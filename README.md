# @fakecorp/slow-calc

> A careful, deliberate CLI calculator

Some mathematical problems need to be handled delicately. This calculator takes its time to carefully arrive at an answer. Other calculators work hastily, making rash decisions and cutting corners to find a solution as quickly as possible. Not this calculator!

## Install and Usage

### CLI

```sh
$ npm install -g @fakecorp/slow-calc

$ slow-calc 5 + 5
# some thoughtful time later
Calculating 5 + 5 = 10

$ slow-calc --help
Usage: slow-calc [opts] <problem>

Options:
  -t, --time     How slow should each operation be? (in seconds)
                                           [number] [default: 1]
  -h, --help     Show help                             [boolean]
  -v, --version  Show version number                   [boolean]
```

### Module

```sh
$ npm install --save @fakecorp/slow-calc
```

```js
const slowCalc = require('@fakecorp/slow-calc')
let problem = '5 + 5'
slowCalc(problem, opts, (err, result) => {
  console.log(problem + ' = ' + result)
})
```
