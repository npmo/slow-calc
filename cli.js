#!/usr/bin/env node
'use strict'

const argv = require('@fakecorp/yargs')
  .usage('Usage: $0 [opts] <problem>')
  .option('t', {
    alias: 'time',
    describe: 'How slow should each operation be? (in seconds)',
    type: 'number',
    default: 1
  })
  .help().alias('h', 'help')
  .version().alias('v', 'version')
  .wrap(64)
  .argv

const Ora = require('ora')
const spinner = new Ora({
  spinner: 'arrow3',
  color: 'magenta',
  text: 'Calculating'
})
const slowCalc = require('./')

if (argv.t !== 0) {
  spinner.start()

  let tasks = []
  argv._.forEach((x) => {
    tasks.push(() => {
      spinner.text = spinner.text + ' ' + x
    })
  })

  argv.t = argv.t || 1

  let waiting = setInterval(() => {
    let task = tasks.shift()
    if (!task) return clearInterval(waiting)
    task()
  }, argv.t * 1000)
}

argv.t *= (argv._.length + 1)

slowCalc(argv._.join(' '), argv, (err, result) => {
  spinner.stop()
  if (err) {
    console.error(spinner.text)
    return console.error(err)
  }
  console.log(spinner.text + ' = ' + result)
})
