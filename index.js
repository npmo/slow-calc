'use strict'

module.exports = function (problem, opts, cb) {
  if (typeof opts === 'function') {
    cb = opts
    opts = {}
  }

  if (opts.t !== 0) opts.t = opts.t || 5

  try {
    setTimeout(() => {
      let r
      try {
        r = eval(problem)
      } catch (e) {
        return cb(e)
      }
      cb(null, r)
    }, opts.t * 1000)
  } catch (e) {
    process.nextTick(() => {
      cb(e)
    })
  }
}
